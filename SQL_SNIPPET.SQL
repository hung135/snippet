DROP_PROCEDURE_SQL = """
DECLARE @name VARCHAR(128)
DECLARE @schema_name VARCHAR(128)
DECLARE @SQL NVARCHAR(500)

-- Array of schema names
DECLARE @schemas TABLE (id INT IDENTITY(1,1), name VARCHAR(128))
INSERT INTO @schemas(name) VALUES ('model1'), ('model2')

-- Iterator variable
DECLARE @i INT = 1

WHILE @i <= (SELECT MAX(id) FROM @schemas)
BEGIN
    -- Get the current schema name
    SELECT @schema_name = name FROM @schemas WHERE id = @i

    -- Get the first procedure from the current schema
    SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = @schema_name ORDER BY [name])

    -- Drop all procedures from the current schema
    WHILE @name IS NOT NULL
    BEGIN
        SET @SQL = 'DROP PROCEDURE ' + QUOTENAME(@schema_name) + '.' + QUOTENAME(@name)
        EXEC sp_executesql @SQL
        PRINT 'Dropped Procedure: ' + @name
        SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE [name] > @name AND SCHEMA_NAME(schema_id) = @schema_name ORDER BY [name])
    END

    -- Move on to the next schema
    SET @i = @i + 1
END
"""
