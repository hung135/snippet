import os
import glob
import pyodbc

def execute_ddl_statements(file_path, engine):
    # Open and read the sql file
    with open(file_path, 'r') as file:
        ddl_statement = file.read()

    # Ensure the engine is connected
    if not engine.is_connected():
        engine.connect()

    cursor = engine.cursor()

    try:
        # Execute the sql statements read from file
        cursor.execute(ddl_statement)
        # Commit if execution was successful
        engine.commit()
        print(f"DDL statement from '{file_path}' executed successfully.")
    except pyodbc.Error as e:
        # Rollback in case of error
        print(f"Error executing DDL statement from '{file_path}': {e}")
        engine.rollback()
    finally:
        # Close cursor after execution
        cursor.close()

def main():
    # Connect to the database using pyodbc
    engine = pyodbc.connect('your_existing_connection_string')

    # Look for all .sql files in the current directory
    files_to_process = glob.glob("*.sql")  

    for file_path in files_to_process:
        if os.path.exists(file_path):
            # Execute the sql statements from each file
            execute_ddl_statements(file_path, engine)
        else:
            print(f"File '{file_path}' not found.")

    # Close the database connection
    engine.close()

if __name__ == "__main__":
    main()
