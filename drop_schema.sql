DECLARE @schema varchar(50)
SET @schema = 'YourSchema' -- replace with your schema name

DECLARE @sql varchar(max) = ''

-- drop all tables
SELECT @sql = @sql + 'DROP TABLE [' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']; ' 
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = @schema

-- drop all views
SELECT @sql = @sql + 'DROP VIEW [' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']; ' 
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'VIEW' AND TABLE_SCHEMA = @schema

-- drop all procedures
SELECT @sql = @sql + 'DROP PROCEDURE [' + specific_schema + '].[' + specific_name + ']; '
FROM INFORMATION_SCHEMA.ROUTINES 
WHERE routine_type = 'PROCEDURE' AND specific_schema = @schema

-- drop all functions
SELECT @sql = @sql + 'DROP FUNCTION [' + specific_schema + '].[' + specific_name + ']; '
FROM INFORMATION_SCHEMA.ROUTINES 
WHERE routine_type = 'FUNCTION' AND specific_schema = @schema

EXEC (@sql)

-- now you can drop the schema
EXEC('DROP SCHEMA [' + @schema + ']')
