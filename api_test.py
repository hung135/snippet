import http.client
import urllib.parse
import json
import ssl
import os
import logging
from typing import Optional, Dict, Any

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Load configuration
from config import INITIAL_LOGIN_URL, VERIFY_SSL, API_BASE_URL

class AuthenticationError(Exception):
    pass

class APIClient:
    def __init__(self):
        self.cookies = {}
        self.token = None
        self.login_url = None
        self.form_inputs = None

    def get_login_page_info(self) -> None:
        try:
            parsed_url = urllib.parse.urlparse(INITIAL_LOGIN_URL)
            conn = self._get_connection(parsed_url)
            
            conn.request("GET", parsed_url.path)
            response = conn.getresponse()
            
            if response.status == 302:  # Handle redirect
                location = response.getheader('Location')
                if location:
                    self.login_url = urllib.parse.urljoin(INITIAL_LOGIN_URL, location)
                    parsed_url = urllib.parse.urlparse(self.login_url)
                    conn = self._get_connection(parsed_url)
                    conn.request("GET", parsed_url.path)
                    response = conn.getresponse()
            
            content = response.read().decode('utf-8')
            
            # Simple form parsing
            form_start = content.find('<form')
            form_end = content.find('</form>', form_start)
            if form_start == -1 or form_end == -1:
                raise AuthenticationError("Login form not found on the page")
            
            form_content = content[form_start:form_end]
            
            # Extract action URL
            action_start = form_content.find('action="') + 8
            action_end = form_content.find('"', action_start)
            form_action = form_content[action_start:action_end]
            self.login_url = urllib.parse.urljoin(INITIAL_LOGIN_URL, form_action)
            
            # Extract input fields
            self.form_inputs = {}
            input_tags = form_content.split('<input')
            for input_tag in input_tags[1:]:
                name_start = input_tag.find('name="') + 6
                name_end = input_tag.find('"', name_start)
                if name_start != -1 and name_end != -1:
                    input_name = input_tag[name_start:name_end]
                    value_start = input_tag.find('value="')
                    if value_start != -1:
                        value_start += 7
                        value_end = input_tag.find('"', value_start)
                        input_value = input_tag[value_start:value_end]
                    else:
                        input_value = ''
                    self.form_inputs[input_name] = input_value

            logger.info(f"Login URL: {self.login_url}")
            logger.info(f"Form inputs: {self.form_inputs}")

        except Exception as e:
            logger.error(f"Failed to fetch login page: {str(e)}")
            raise

    def authenticate(self) -> bool:
        try:
            self.get_login_page_info()

            # Get credentials from environment variables
            username = os.environ.get('USERNAME')
            password = os.environ.get('PASSWORD')

            if not username or not password:
                raise AuthenticationError("Username or password not set in environment variables")

            # Update form inputs with credentials
            self.form_inputs.update({
                'username': username,
                'password': password,
            })

            parsed_url = urllib.parse.urlparse(self.login_url)
            conn = self._get_connection(parsed_url)

            headers = {
                "Content-Type": "application/x-www-form-urlencoded",
                "Cookie": '; '.join([f"{k}={v}" for k, v in self.cookies.items()])
            }

            body = urllib.parse.urlencode(self.form_inputs)
            
            conn.request("POST", parsed_url.path, body=body, headers=headers)
            response = conn.getresponse()

            content = response.read().decode('utf-8')
            self._update_cookies(response)

            self.token = self._get_token(content)

            if self.token:
                logger.info("Authenticated successfully!")
                return True
            else:
                logger.error("Token not found in the response.")
                return False

        except Exception as e:
            logger.error(f"Authentication failed: {str(e)}")
            return False

    @staticmethod
    def _get_token(response_text: str) -> Optional[str]:
        token_start = response_text.find('name="token" value="')
        if token_start != -1:
            token_start += 20
            token_end = response_text.find('"', token_start)
            return response_text[token_start:token_end]
        return None

    def call_api(self, endpoint: str, method: str = 'GET', payload: Optional[Dict[str, Any]] = None) -> Optional[Dict[str, Any]]:
        if not self.token:
            logger.error("Not authenticated. Call authenticate() first.")
            return None

        url = urllib.parse.urljoin(API_BASE_URL, endpoint)
        parsed_url = urllib.parse.urlparse(url)
        conn = self._get_connection(parsed_url)

        headers = {
            "Authorization": f"Bearer {self.token}",
            "Content-Type": "application/json",
            "Cookie": '; '.join([f"{k}={v}" for k, v in self.cookies.items()])
        }

        try:
            if method.upper() == 'GET':
                conn.request("GET", parsed_url.path, headers=headers)
            elif method.upper() == 'POST':
                body = json.dumps(payload) if payload else ''
                conn.request("POST", parsed_url.path, body=body, headers=headers)
            else:
                logger.error(f"Unsupported HTTP method: {method}")
                return None

            response = conn.getresponse()
            self._update_cookies(response)
            content = response.read().decode('utf-8')
            return json.loads(content)

        except Exception as e:
            logger.error(f"API call failed: {str(e)}")
            return None

    def _get_connection(self, parsed_url):
        if parsed_url.scheme == 'https':
            context = ssl.create_default_context()
            if not VERIFY_SSL:
                context.check_hostname = False
                context.verify_mode = ssl.CERT_NONE
            return http.client.HTTPSConnection(parsed_url.netloc, context=context)
        else:
            return http.client.HTTPConnection(parsed_url.netloc)

    def _update_cookies(self, response):
        cookies = response.getheader('Set-Cookie')
        if cookies:
            for cookie in cookies.split(','):
                if '=' in cookie:
                    name, value = cookie.strip().split('=', 1)
                    self.cookies[name] = value.split(';')[0]