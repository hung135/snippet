import pyodbc

# Replace these with your actual database credentials
server = 'your_server_name'
database = 'your_database_name'
username = 'your_username'
password = 'your_password'

# Replace 'YourStoredProcedure' with the actual name of your stored procedure
stored_procedure_name = 'YourStoredProcedure'

# Date parameter string
date_string = '2023-07-01'

# Create a connection string
connection_string = (
    f'DRIVER=ODBC Driver 17 for SQL Server;'
    f'SERVER={server};DATABASE={database};'
    f'UID={username};PWD={password};'
)

try:
    # Connect to the database
    conn = pyodbc.connect(connection_string)
    cursor = conn.cursor()

    # Execute the stored procedure with the date parameter
    cursor.execute(f"EXEC {stored_procedure_name} @DateParameter = ?", date_string)
    conn.commit()

    print(f"Stored procedure '{stored_procedure_name}' executed successfully with date '{date_string}'.")

except pyodbc.Error as e:
    print(f"Error: {e}")

finally:
    # Close the connection
    conn.close()
