SELECT 
    t.name AS TableName,
    c.name AS ColumnName,
    tp.name AS DataType,
    c.is_nullable AS IsNullable
FROM 
    sys.tables t
INNER JOIN 
    sys.columns c ON t.object_id = c.object_id
INNER JOIN 
    sys.types tp ON c.user_type_id = tp.user_type_id
WHERE 
    t.name = 'Your_Table_Name' --replace this with your table name
ORDER BY 
    TableName, 
    column_id;
WHERE 
    SCHEMA_NAME(t.schema_id) = 'Your_Schema_Name'
