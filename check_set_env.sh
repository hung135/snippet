#!/bin/bash

# Check if REG_TOKEN environment variable is set
if [ -z "$REG_TOKEN" ]; then
  echo "REG_TOKEN environment variable is not set."
  echo "Please enter the registration token:"
  read -r token
  export REG_TOKEN="$token"
  echo "REG_TOKEN has been set."
else
  echo "REG_TOKEN is already set."
fi
