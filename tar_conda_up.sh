#!/bin/bash

# Name of the environment
ENV_NAME="py39"

# Path where your conda environments are stored
# Usually this would be something like /home/username/miniconda3/envs
# Make sure to replace /path/to/conda/envs with the actual path
CONDA_ENVS_PATH="/path/to/conda/envs"

# Output file names
YAML_FILE="${ENV_NAME}_env.yaml"
TAR_FILE="${ENV_NAME}_env.tar.gz"

# Create a yaml file that contains the environment configuration
conda env export --name $ENV_NAME > $YAML_FILE

# Tar up the environment directory and yaml file
tar -czvf $TAR_FILE -C $CONDA_ENVS_PATH $ENV_NAME $YAML_FILE

echo "Created tarball ${TAR_FILE} containing conda environment ${ENV_NAME} and yaml file ${YAML_FILE}"
